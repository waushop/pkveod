import React, { Component } from "react";
import Table from 'react-bootstrap/Table';


export class Prices extends Component {
  render() {
    return (
      <div id="prices" className="text-center">
        <div className="container">
          <div className="col-md-10 col-md-offset-1 section-title">
            <h2>Hinnad</h2>
            <p>
              Hinnad meie poolt pakutavatele teenustele
            </p>
          </div>
        <div className="container">
          <Table striped bordered hover>
            <thead>
              <tr>
                <th colSpan="2">Teenus</th>
                <th>Hind</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colSpan="2">Veoteenus Tallinnas</td>
                <td>17€/h</td>
              </tr>
              <tr>
                <td colSpan="2">Veoteenus haagisega</td>
                <td>20€/h</td>
              </tr>
              <tr>
                <td colSpan="2">Maanteevedu</td>
                <td>0,50€/km</td>
              </tr>
              <tr>
                <td colSpan="2">Veoteenus 2 mehega</td>
                <td>25€/h</td>
              </tr>
              <tr>
                <td colSpan="2">Heakorrateenus 1 mehega</td>
                <td>20€/h</td>
              </tr>
              <tr>
                <td colSpan="2">Heakorrateenus 2 mehega</td>
                <td>35€/h</td>
              </tr>
            </tbody>
          </Table>
          <div className="row">
            <p>
              *Kõikidele hindadele lisandub käibemaks
            </p>
          </div>
        </div>
          {/*<div className="row">
            {this.props.data
              ? this.props.data.map((d,i) => (
                  <div  key={`${d.title}-${i}`} className="col-xs-6 col-md-3">
                    {" "}
                    <i className={d.icon}></i>
                    <h3>{d.title}</h3>
                    <p>{d.text}</p>
                  </div>
                ))
              : "Loading..."}
          </div>*/}
        </div>
      </div>
    );
  }
}

export default Prices;
