import React from "react";

export default class ContactForm extends React.Component {
  constructor(props) {
    super(props);
    this.submitForm = this.submitForm.bind(this);
    this.state = {
      status: ""
    };
  }

  render() {
    const { status } = this.state;
    return (
      <form onSubmit={this.submitForm} action="https://formspree.io/myybgnpd" method="POST">
        <div className="row">
            <div className="col-md-6">
                <div className="form-group">
                <input type="text" id="name" name="name" className="form-control" placeholder="Nimi" required="required"/>
                <p className="help-block text-white"></p>
                </div>
            </div>
            <div className="col-md-6">
                <div className="form-group">
                <input type="text" id="company" name="company" className="form-control" placeholder="Ettevõtte nimi" required="required"/>
                <p className="help-block text-white"></p>
                </div>
            </div>
            </div>
            <div className="row">
            <div className="col-md-6">
                <div className="form-group">
                <input type="email" id="email" name="email" className="form-control" placeholder="E-mail" required="required"/>
                <p className="help-block text-white"></p>
                </div>
            </div>
            <div className="col-md-6">
                <div className="form-group">
                    <input type="text" id="phone" name="phone" className="form-control" placeholder="Telefon" required="required"/>
                    <p className="help-block text-white"></p>
                </div>
            </div>
        </div>
        <div className="form-group">
            <textarea name="message" id="message" className="form-control" rows="6" placeholder="Sõnumi sisu" required></textarea>
            <p className="help-block text-white"></p>
        </div>
        {status === "SUCCESS" && <p className="alert alert-success" role="alert"><strong>Aitäh!</strong> Sõnum on kohale toimetatud. Võtame teiega ühendust esimesel võimalusel.</p>}
        {status === "ERROR" && <p className="alert alert-danger" role="alert"><strong>Oeh!</strong> Paistab,et midagi läks valesti. Proovi uuesti või helista meile.</p>}
        <button type="submit" className="btn btn-custom btn-lg">Saada</button>  
      </form>
    );
  }

  submitForm(ev) {
    ev.preventDefault();
    const form = ev.target;
    const data = new FormData(form);
    const xhr = new XMLHttpRequest();
    xhr.open(form.method, form.action);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.onreadystatechange = () => {
      if (xhr.readyState !== XMLHttpRequest.DONE) return;
      if (xhr.status === 200) {
        form.reset();
        this.setState({ status: "SUCCESS" });
      } else {
        this.setState({ status: "ERROR" });
      }
    };
    xhr.send(data);
  }
}