import React, { Component } from 'react'
import Navigation from './components/Navigation';
import Header from './components/Header';
import Prices from './components/Prices';
import About from './components/About';
import Services from './components/Services';
import Contact from './components/Contact';
import JsonData from './data/data.json';

export class App extends Component {
  state = {
    landingPageData: {},
  }
  getlandingPageData() {
    this.setState({landingPageData : JsonData})
  }

  componentDidMount() {
    this.getlandingPageData();
  }

  render() {
    return (
      <div>
        <Navigation />
        <Header data={this.state.landingPageData.Header} />
        <About data={this.state.landingPageData.About} />
        <Services data={this.state.landingPageData.Services} />
        <Prices data={this.state.landingPageData.Prices} />
        <Contact data={this.state.landingPageData.Contact} />
      </div>
    )
  }
}

export default App;
